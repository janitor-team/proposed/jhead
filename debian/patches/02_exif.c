From: Ludovic Rousseau <rousseau@debian.org>
Subject: fix crash
Bug-Debian: https://bugs.debian.org/967924

--- a/exif.c
+++ b/exif.c
@@ -26,6 +26,7 @@
     char * Desc;
 }TagTable_t;
 
+#define min(a, b) ((a)<(b) ? (a) : (b))
 
 //--------------------------------------------------------------------------
 // Table of Jpeg encoding process names
@@ -1047,7 +1048,8 @@
         printf("Map: %05d- End of exif\n",length-8);
         for (a=0;a<length-8;a+= 10){
             printf("Map: %05d ",a);
-            for (b=0;b<10;b++) printf(" %02x",*(ExifSection+8+a+b));
+            for (b=0;b<min(10, length-8-a);b++)
+				printf(" %02x",*(ExifSection+8+a+b));
             printf("\n");
         }
     }
