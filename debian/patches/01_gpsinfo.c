From: Ludovic Rousseau <rousseau@debian.org>
Subject: fix crash
Bug-Debian: https://bugs.debian.org/953352

--- a/gpsinfo.c
+++ b/gpsinfo.c
@@ -101,7 +101,7 @@
             unsigned OffsetVal;
             OffsetVal = Get32u(DirEntry+8);
             // If its bigger than 4 bytes, the dir entry contains an offset.
-            if (OffsetVal > 0x1000000 || OffsetVal+ByteCount > ExifLength){
+            if (OffsetVal > 0x1000000 || ByteCount > INT_MAX || OffsetVal+ByteCount > ExifLength){
                 // Max exif in jpeg is 64k, so any offset bigger than that is bogus.
                 // Bogus pointer offset and / or bytecount value
                 ErrNonfatal("Illegal value pointer for Exif gps tag %04x", Tag,0);
